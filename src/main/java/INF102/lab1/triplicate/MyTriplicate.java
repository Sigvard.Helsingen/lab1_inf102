package INF102.lab1.triplicate;
import java.util.HashMap;
import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        HashMap<T, Integer> myHashmap = new HashMap<T, Integer>();
        for (int i = 0; i < list.size(); i++){
            if (myHashmap.containsKey(list.get(i)) == true){
                myHashmap.put(list.get(i), myHashmap.get(list.get(i))+1);
                if (myHashmap.get(list.get(i)) == 3){
                    return list.get(i);
                }
            }
            else{
                myHashmap.put(list.get(i), 1);
            }
        }

        return null;
    }  
}
